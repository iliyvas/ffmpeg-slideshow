import argparse
import logging
import os
import shutil
import sys
from PIL import Image
import subprocess
from multiprocessing.pool import ThreadPool

ALLOWED_EXTENSION = ['.jpg', '.png']
FFMPEG = 'ffmpeg'
MAX_PROCESS_NUM = 4
MAX_SIZE = (1920 * 3, 1080 * 3)

logging.basicConfig(filename='log', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s: %(message)s', datefmt="%Y-%m-%d %H:%M:%S")


def resize_image(image):
    s = image.size
    width_r = MAX_SIZE[0] / s[0]
    height_r = MAX_SIZE[1] / s[1]
    ratio = min(width_r, height_r)
    return image.resize((s[0] * ratio, s[1] * ratio))


def resize_image_file(img_path, filename, resized_images_path):
    im = Image.open(img_path)
    im = resize_image(im)
    im.save(os.path.join(resized_images_path, filename))


def renew_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder, ignore_errors=True)

    os.makedirs(folder)


def preprocess_images_in_folder(image_folder, output_folder):
    """
    Resize all images from the folder to the same size
    """
    resized_images_path = os.path.join(output_folder, '.resized_images')
    try:
        renew_folder(resized_images_path)

        no_image_files = True
        tp = ThreadPool(MAX_PROCESS_NUM)
        for filename in os.listdir(image_folder):
            if os.path.splitext(filename)[1].lower() in ALLOWED_EXTENSION:
                no_image_files = False
                tp.apply_async(resize_image_file, (os.path.join(
                    image_folder, filename), filename, resized_images_path))
                # im = Image.open(os.path.join(image_folder, filename))
                # im = resize_image(im)
                # im.save(os.path.join(resized_images_path, filename))

        if no_image_files:
            extensions = ', '.join(ALLOWED_EXTENSION)
            raise Exception('There is no files with ' +
                            extensions + ' extensions in ' + image_folder)
        else:
            tp.close()
            tp.join()

    except Exception as e:
        logging.error(str(e))
        sys.exit()

    return resized_images_path


def bake_sweet_slideshow(images_folder, sound_path, background_path, watermark_path, output_name, quality, nvenc):
    """
    Concat partitions videos, add watermark, background video and nice soundtrack
    """
    command = [
        FFMPEG,
        '-threads', '1'
    ]

    part_num = 0
    filter_complex = 'movie=%s:loop=0,setpts=N/FRAME_RATE/TB[bg];' % background_path
    filter_complex += 'movie=%s:loop=0,setpts=N/FRAME_RATE/TB[wm];' % watermark_path
    concat_img_videos = ''
    for filename in os.listdir(images_folder):
        part_num += 1
        filename_abs_path = os.path.join(images_folder, filename)
        command.extend(['-loop', '1', '-t', '3', '-i', filename_abs_path])

        template = """[<num>]format=rgba,pad=if(lt(iw/3\,1920)\,if(lt(ih/3\,1080)\,iw*1.1*(1920/(iw/3))\,iw*2.21)\,iw*1.3):if(lt(iw/3\,1920)\,if(lt(ih/3\,1080)\,ih*1.1*(1920/(iw/3))*((iw/ih)/1.7777)\,ih*2.21*((iw/ih)/1.7777))\,ih*1.3*((iw/ih)/1.7777)):(ow-iw)/2:(oh-ih)/2:color=black@0,
                                    zoompan=z='zoom+0.0015':s=hd""" + quality + """:d=25*3:x='iw/2-(iw/zoom/2)':y='ih/2-(ih/zoom/2)',
                                    fade=t=in:0:25:alpha=1,split[img<num>1][img<num>2];
                      [img<num>1]trim=start=0:end=2,setpts=PTS-STARTPTS[img<num>start];
                      [img<num>2]trim=start=2:end=3,setpts=PTS-STARTPTS[img<num>end];
                      [img<num>end]fade=t=out:0:25:alpha=1[img<num>fade];
                      [img<num>start][img<num>fade]concat=n=2:v=1:a=0[img<num>res];"""

        filter_complex += template.replace('<num>', str(part_num - 1))
        concat_img_videos += '[img%sres]' % str(part_num - 1)

    filter_complex += ('amovie={0}:loop=0,afade=in:st=0:d=2,afade=out:st={1}:d=1,asetpts=N/SR/TB,' +
                       'atrim=duration={2}[sound];').format(sound_path, part_num * 3 - 1, part_num * 3)
    filter_complex += concat_img_videos + \
        'concat=n=%s:v=1:a=0' % str(part_num) + '[o1];'
    filter_complex += '[bg][o1]overlay=(W-w)/2:(H-h)/2:shortest=1[o2];'
    filter_complex += '[o2][wm]overlay=10:main_h-overlay_h-10:shortest=1[res]'
    command.extend(['-filter_complex', filter_complex])

    command.extend([
        '-map', '[res]',
        '-map', '[sound]'
    ])

    if nvenc:
        command.extend(['-c:v', 'h264_nvenc', '-surfaces', '2'])
    else:
        command.extend(['-c:v', 'libx264'])

    command.extend([
        '-s', 'hd' + quality,
        '-threads', '1',
        '-strict', '-2',
        '-y', output_name
    ])

    logging.debug(' '.join(command))

    try:
        subprocess.check_call(command, stdin=open(os.devnull))
    except subprocess.CalledProcessError as e:
        logging.error(e.output)
        sys.exit()


def main():
    parser = argparse.ArgumentParser(description='Bake slideshow.')
    parser.add_argument(
        '-o', '--output', help='Specify output filename', required=True)
    parser.add_argument(
        '-s', '--source', help='Specify input images folder', required=True)
    parser.add_argument('-b', '--background',
                        help='Path to background video', required=True)
    parser.add_argument(
        '-m', '--music', help='Path to soundtrack', required=True)
    parser.add_argument('-w', '--watermark',
                        help='Path to watermark', required=True)
    parser.add_argument('-q', '--quality',
                        help='Choose 720 or 1080', required=True)
    parser.add_argument('-n', '--nvenc', help='Enable nvenc')
    args = parser.parse_args()

    source_folder = os.path.abspath(args.source)
    background_path = os.path.abspath(args.background)
    music_path = os.path.abspath(args.music)
    watermark_path = os.path.abspath(args.watermark)

    for f in [source_folder, background_path, music_path, watermark_path]:
        if not os.path.exists(f):
            logging.error(f + ' doesn\'t exist')
            sys.exit()

    if args.quality not in ['720', '1080']:
        logging.error('Quality value should be 720 or 1080')
        sys.exit()

    if args.nvenc is None:
        nvenc = False
    else:
        nvenc = True

    images_folder = preprocess_images_in_folder(source_folder, source_folder)
    bake_sweet_slideshow(images_folder,
                         music_path,
                         background_path,
                         watermark_path,
                         args.output,
                         args.quality,
                         nvenc)


if __name__ == '__main__':
    main()
